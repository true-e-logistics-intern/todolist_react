# base image
FROM node:13.12.0-alpine

# set working directory
WORKDIR /app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json ./
COPY package-lock.json ./

RUN npm install --silent
RUN npm install react-scripts -g --silent

# Copy the current directory contents innto container /
COPY . ./


EXPOSE 9000

# This allows Heroku bind its PORT the Apps port
# since Heroku needs to use its own PORT before the App can be made accessible to the world
EXPOSE $PORT

# start app
CMD ["npm", "start"]
