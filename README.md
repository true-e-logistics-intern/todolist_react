# React To-Do List

This is a simple To-Do list app done with React. All tasks are saved into browser's local storage only. The app development

## Demo

[https://adoring-lamport-00388d.netlify.app/](https://adoring-lamport-00388d.netlify.app/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes.

### Prerequisites

The project can be built with npm or yarn, so choose one of the approach bellow in case you don't
have any installed on your system.

- npm is distributed with Node.js which means that when you download Node.js,
  you automatically get npm installed on your computer. [Download Node.js](https://nodejs.org/en/download/)

or

- Yarn is a package manager built by Facebook Team and seems to be faster than npm in general. [Download Yarn](https://yarnpkg.com/en/docs/install)

### Installing

To download the project follow the instructions bellow

```
git clone https://gitlab.com/true-e-logistics-intern/todolist_react.git
cd todolist_react
```

Install dependencies and run with:

npm

```
npm install
npm start
```

or

yarn

```
yarn install
yarn start
```

Your app should now be running on [localhost:9000](http://localhost:9000/)

## Project Todolist Structure

```
.
├── node_modules
├── public
|   ├── favicon.ico
|   ├── index.html
|   ├── logo192.png
|   ├── logo512.png
|   ├── manifest.json
|   └── robots.txt
├── scr
|   ├──component
|   |   ├── add-taskcomponent.js
|   |   ├── List-item.js
|   |   ├── SearchTask.js
|   |   ├── tasks.component.js
|   |   └── task-list.component.js
|   ├──service
|   |   ├── http-common.js
|   |   └── task.service.js
|   ├──App.css
|   ├──App.js
|   ├──App.test.js
|   ├──index.css
|   ├──index.js
|   ├──logo.svg
|   ├──serviceWorker.js
|   └──setupTests.js
├── .dokerignore
├── .env
├── .eslintrc
├── .gitignore
├── .prottierrc
├── Dockerfile
├── package.json
└── package-lock.json

```

## List of Packages

| Package            | Description                                                                                                  |
| ------------------ | ------------------------------------------------------------------------------------------------------------ |
| axios              | Requests can be made by passing the relevant config to axios.                                                |
| bootstrap          | Bootstrap is a framework to help you design websites faster and easier                                       |
| react              | React makes it painless to create interactive UIs.                                                           |
| react-animated-css | React component to show or hide elements with animations using Animated.                                     |
| react-dom          | Render a React element into the DOM in the supplied container.                                               |
| react-loading      | Easy to use loading animations for React projects. Uses SVG animations from Brent Jackson's loading project. |
| react-router-dom   | DOM bindings for React Router.                                                                               |
| react-scripts      | This package includes scripts and configuration used by Create React App.                                    |
| styled-components  | This component is a lightweight, simple line style component.                                                |
| prettier           | Prettier is an opinionated code formatter                                                                    |
