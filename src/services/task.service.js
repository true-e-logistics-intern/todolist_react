import http from './http-common'

class TaskDataService {
  getAll(queryValue) {
    if (queryValue) {
      return http.get(`/tasks?title=${queryValue}`)
    } else {
      return http.get('/tasks')
    }
  }

  get(id) {
    return http.get(`/tasks/${id}`)
  }

  create(data) {
    return http.post('/tasks', data)
  }

  update(id, data) {
    return http.patch(`/tasks/${id}`, data)
  }

  delete(id) {
    return http.delete(`/tasks/${id}`)
  }
  completeBydone(id, done) {
    return http.patch(`/tasks/complete/${id}`, { done: done })
  }
}

const Service = new TaskDataService()
export default Service
