import axios from 'axios'

export default axios.create({
  baseURL: 'https://koa-webserver-staging.herokuapp.com/api',
  headers: {
    'Content-type': 'application/json',
  },
})
