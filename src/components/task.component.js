import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react'
import Service from '../services/task.service.js'

export const Task = forwardRef((props, ref) => {
  const { data, setCustomState, retrieveTasks } = props
  const [initial, setInitial] = useState(data)

  useImperativeHandle(ref, () => {
    return {
      triggerTask: (e, id) => {
        completeTask(e, id)
      },
    }
  })
  const onSetTask = (e, key) => {
    setInitial({
      ...initial,
      [key]: e.target.value,
    })
  }

  const deleteTask = async (e, id) => {
    try {
      setCustomState('loading', true)
      await Service.delete(id)
      setCustomState('currentIndex', -1)
      setCustomState('loading', false)
      setCustomState('opened', false)
      retrieveTasks()
    } catch (error) {
      console.error(`${error}`)
    }
  }

  const completeTask = async (e, id) => {
    try {
      let completed = e.target.value
      let flag = { done: false }
      if (completed) {
        flag.done = !flag.done
        setCustomState('currentIndex', -1)
      }
      await Service.completeBydone(id, flag)

      retrieveTasks()
    } catch (error) {
      console.error(`${error}`)
    }
  }

  const updateTask = async (data) => {
    try {
      const { _id, ...taskObj } = data
      setCustomState('loading', true)
      const updatebytask = await Service.update(_id, taskObj)
      setInitial(updatebytask.data.data)
      setCustomState('opened', false)
      setTimeout(() => {
        setCustomState('currentIndex', -1)
        setCustomState('loading', false)
        retrieveTasks()
      }, 2000)
    } catch (error) {
      console.error(`${error}`)
    }
  }

  useEffect(() => {
    setInitial(data)
  }, [data])

  return (
    <div>
      <div className="edit-form ">
        <h4>Title {initial.title}</h4>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            id="title"
            value={initial.title}
            onChange={(e) => onSetTask(e, 'title')}
            name="title"
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <textarea
            type="text"
            className="form-control"
            rows="3"
            id="description"
            value={initial.description}
            onChange={(e) => onSetTask(e, 'description')}
            name="description"
          />
        </div>
        <div class="singletask">
          <button type="submit" className="badge badge-warning mr-2" onClick={() => updateTask(initial)}>
            Update
          </button>
          <button type="submit" className="badge badge-danger mr-2" onClick={(e) => deleteTask(e, initial._id)}>
            Delete
          </button>
        </div>
      </div>
    </div>
  )
})
