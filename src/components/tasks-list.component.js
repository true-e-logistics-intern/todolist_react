import React, { useState, useEffect, useRef } from 'react'
import Service from '../services/task.service.js'
import { Task } from './task.component'
import ReactLoading from 'react-loading'
import { SearchTask } from './SearchTask'
import { Listitem } from './List-item'
import { AddTask } from './add-task.component'
import styled from 'styled-components'

const Widthbar = styled.div`
  padding-bottom: 50px;
  .widthbar {
    width: ${(props) => props.width}%;
  }
`
export const TaskList = () => {
  const [initial, setInitial] = useState({
    Percents: [],
    tasks: [],
    currentTask: null,
    currentIndex: -1,
    loading: false,
    opened: false,
    toggleAddTaskList: false
  })

  const taskRef = useRef()
  const overLayRef = useRef(null)

  const handleClickOutSideTask = (event) => {
    const element = event.target
    if (overLayRef.current && !overLayRef.current.contains(element)) {
      setCustomState('currentIndex', -1)
      setCustomState('opened', false)
      retrieveTasks()
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutSideTask)
    return () => {
      document.removeEventListener('mousedown', handleClickOutSideTask)
    }
  })
  const searchByTitle = async (data) => {
    try {
      const { searchTitle } = data
      const title = await Service.getAll(searchTitle)
      setInitial((prevState) => ({
        ...prevState,
        tasks: title.data.data.allTasks
      }))
    } catch (error) {
      console.error(`${error}`)
    }
  }

  const retrieveTasks = async () => {
    try {
      const getalltask = await Service.getAll()
      setInitial((prevState) => ({
        ...prevState,
        tasks: getalltask.data.data.allTasks,
        Percents: getalltask.data.data.donePercent
      }))
    } catch (error) {
      console.error(`${error}`)
    }
  }

  const setCustomState = (stateKey, stateValue) => {
    setInitial((prevState) => ({
      ...prevState,
      [stateKey]: stateValue
    }))
  }

  useEffect(() => {
    retrieveTasks()
  }, [])

  const { tasks, currentTask, currentIndex, opened, loading, toggleAddTaskList } = initial
  return (
    <div className="list row ">
      <div className="col-md-12">
        <SearchTask onSearch={searchByTitle} />
        <button
          className="btn btn-outline-secondary magintasklist"
          onClick={() => setCustomState('toggleAddTaskList', !toggleAddTaskList)}
        >
          Add Task List +
        </button>
        {toggleAddTaskList && <AddTask setCustomState={setCustomState} retrieveTasks={retrieveTasks} />}
      </div>
      <div className="col-md-6">
        <Widthbar width={initial.Percents}>
          <div className="progress">
            <div className="progress-bar widthbar" role="progressbar">
              {initial.Percents}%
            </div>
          </div>
        </Widthbar>
        <h4>Task Lists</h4>
        {loading ? (
          <div>
            <br />
            <ReactLoading
              type={'spinningBubbles'}
              color={'#77aaff'}
              height={'25%'}
              width={'25%'}
              className={'justify-content-center'}
            />
          </div>
        ) : (
          <div className="" ref={overLayRef}>
            <Listitem
              data={tasks}
              currentIndex={currentIndex}
              setInitial={setInitial}
              onClickTask={(task, index) => {
                if (!task.done) {
                  setInitial((prevState) => ({
                    ...prevState,
                    opened: true,
                    currentTask: task,
                    currentIndex: index
                  }))
                }
              }}
              triggerTask={async (e, id) => {
                await Service.completeBydone(id, e.target.checked)
                setCustomState('currentIndex', -1)
                setCustomState('opened', false)
                await retrieveTasks()
              }}
            />
          </div>
        )}
      </div>
      <div className="col-md-6">
        {opened ? (
          <div ref={overLayRef}>
            <Task data={currentTask} setCustomState={setCustomState} retrieveTasks={retrieveTasks} ref={taskRef} />
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Task...</p>
          </div>
        )}
      </div>
    </div>
  )
}
