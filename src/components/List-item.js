import React from 'react'

export const Listitem = React.memo((props) => {
  return (
    <ul className="list-group">
      {props.data &&
        props.data.map((task, index) => {
          const checktypebox = task.done === true
          const checked = checktypebox ? { defaultChecked: true } : { defaultChecked: false }
          return (
            <li
              className={
                'list-group-item ' +
                'btn-outline-secondary' +
                // (index === props.currentIndex ? 'action' : '') +
                (task.done === true ? 'list-group-item-action list-group-item-success' : '')
              }
              onClick={() => {
                props.onClickTask(task, index)
              }}
              key={task._id}
            >
              <input
                type="checkbox"
                onChange={(e) => {
                  props.triggerTask(e, task._id)
                }}
                {...checked}
              ></input>
              &ensp;
              {checktypebox ? <s>{task.title}</s> : task.title}
            </li>
          )
        })}
    </ul>
  )
})
