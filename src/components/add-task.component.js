import React, { useState } from 'react'
import Service from '../services/task.service.js'
import styled from 'styled-components'

const ContainerAnimate = styled.div`
  margin: 0px 0px 50px -60px;
  .fade-in {
    animation: fadeIn 1s ease-in both;
  }

  @keyframes fadeIn {
    from {
      opacity: 0;
      transform: translate3d(0, -20%, 0);
    }
    to {
      opacity: 1;
      transform: translate3d(0, 0, 0);
    }
  }
`

export const AddTask = (props) => {
  const { setCustomState, retrieveTasks } = props
  const [task, setTask] = useState({
    id: null,
    title: '',
    description: '',
    done: false,

    submitted: false,
  })

  const saveTask = async () => {
    try {
      let data = {
        title: task.title,
        description: task.description,
        done: task.done,
      }
      setCustomState('loading', true)

      const save = await Service.create(data)

      setTask({
        id: save.data.id,
        title: save.data.title,
        description: save.data.description,
        done: save.data.done,

        submitted: true,
      })
      setCustomState('toggleAddTaskList', false)
      setTimeout(() => {
        setCustomState('loading', false)
        setCustomState('opened', false)
        retrieveTasks()
      }, 2000)
    } catch (error) {
      console.error(`${error}`)
    }
  }

  const onSetTask = (e, key) => {
    setTask({
      ...task,
      [key]: e.target.value,
    })
  }

  return (
    <div>
      <ContainerAnimate>
        <div className="submit-form">
          <div className="form-group fade-in">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              required
              value={task.title || ''}
              onChange={(e) => onSetTask(e, 'title')}
              name="title"
            />
          </div>
          <div className="form-group fade-in">
            <label htmlFor="description">Description</label>
            <textarea
              type="text"
              className="form-control"
              rows="3"
              id="description"
              required
              value={task.description || ''}
              onChange={(e) => onSetTask(e, 'description')}
              name="description"
            ></textarea>
          </div>
          <button onClick={saveTask} className="btn btn-success fade-in">
            Submit
          </button>
        </div>
      </ContainerAnimate>
    </div>
  )
}
