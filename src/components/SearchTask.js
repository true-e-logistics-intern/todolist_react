import React, { useState } from 'react'

export const SearchTask = (props) => {
  const [initial, setInitial] = useState({
    searchTitle: '',
  })
  const onSetState = (e, key) => {
    setInitial({
      ...initial,
      [key]: e.target.value,
    })
  }

  return (
    <div className="input-group mb-3">
      <form
        onSubmit={(e) => {
          e.preventDefault()
          props.onSearch(initial)
        }}
      >
        <div className="input-group-append">
          <input
            type="text"
            className="form-control"
            placeholder="Search by title"
            value={initial.searchTitle}
            onChange={(e) => onSetState(e, 'searchTitle')}
            name="searchTitle"
          />
          <button className="btn btn-outline-secondary" type="submit" onClick={(() => props.onSearch(initial), {})}>
            Search
          </button>
        </div>
      </form>
    </div>
  )
}
