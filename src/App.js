import React from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Task } from './components/task.component'
import { TaskList } from './components/tasks-list.component'

function App() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={'/tasks'} className="navbar-brand">
            My Tasks
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={'/tasks'} className="nav-link">
                All
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Switch>
            <Route exact path={['/', '/tasks']} component={TaskList} />
            <Route path="/tasks/:id" component={Task} />
          </Switch>
        </div>
      </div>
    </Router>
  )
}

export default App
